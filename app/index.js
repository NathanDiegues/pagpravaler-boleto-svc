// ./index.js
const express = require('express');
const mountRoutes = require('./routes');

const app = express();

const port = 8080;

//para processar formulários
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//incia os endpoints
mountRoutes(app);

app.get('/', (req, res) => {
    res.send('Microservice Boleto OK')
});

app.listen(port, () => console.log(`Boleto listening on port ${port}`));