// ./routes/boleto.js
const Router = require('express-promise-router');
var uniqid = require('uniqid')

const db = require('../db');

const router = new Router();

module.exports = router;

//busca boletos pelo id de proposta ou usuário
router.get('/:id', async (req, res) => {
    const { id }  = req.params;
    const { rows } = await db.query(
        "SELECT * FROM pgprv_boleto WHERE pro_id = $1 or usr_id = $1",
        [id]
    );

    res.status(200).send(rows);
})

//registra o boleto
router.post('/register', async (req, res) => {
    const bol_id        = uniqid();
    const pro_id        = req.body.pro_id;
    const usr_id        = req.body.usr_id;
    const numero        = req.body.numero; //numero do boleto
    const valor         = req.body.valor; //valor tem que ser com ponto ex: 100.50
    const vencimento    = req.body.vencimento;
    const created_at    = new Date();

    const { query } = await db.query(
        "INSERT INTO pgprv_boleto(bol_id, pro_id, usr_id, numero, valor, vencimento, created_at) "+
        "VALUES ($1, $2, $3, $4, $5, $6, $7)",
        [bol_id, pro_id, usr_id, numero, valor, vencimento, created_at], (err, result) => {
            if (err) {
                res.status(500).send(err);
                return console.error('Error executing query', err.stack);
            }
        })

    res.status(200).send("Boleto registrado com sucesso. Id = "+ bol_id);
})

//altera proposta do boleto a ser parcelado
router.post('/update', async (req, res) => {
    const bol_id        = req.body.bol_id;
    const numero        = req.body.numero;
    const valor         = req.body.valor;
    const vencimento    = req.body.vencimento;
    const pago          = req.body.pago;
    const updated_at    = new Date();

    const { query } = await db.query(
        "UPDATE pgprv_boleto SET numero = $1, valor = $2, "+
        "vencimento = $3,pago = $4, updated_at = $5 "+
        "WHERE bol_id = $6",
        [numero, valor, vencimento, pago, updated_at, bol_id], (err, result) => {
            if (err) {
                res.status(500).send(err);
                return console.error('Error executing query', err.stack);
            }
        })

    res.status(200).send("Boleto alterado com sucesso.");
})