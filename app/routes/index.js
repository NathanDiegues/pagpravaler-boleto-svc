// ./routes/index.js
const proposta = require('./proposta');
const boleto = require('./boleto');

module.exports = app => {
    app.use('/proposta', proposta);
    app.use('/boleto', boleto);
}