// ./routes/proposta.js
const Router = require('express-promise-router');
var uniqid = require('uniqid')

const db = require('../db');

const router = new Router();

module.exports = router;

//busca proposta pelo id de usuário
router.get('/:id', async (req, res) => {
    const { id }  = req.params;
    const { rows } = await db.query(
        "SELECT * FROM pgprv_proposta WHERE usr_id = $1",
        [id]
    );

    res.status(200).send(rows[0]);
})

//registra proposta do boleto a ser parcelado
router.post('/register', async (req, res) => {
    const pro_id        = uniqid();
    const usr_id        = req.body.usr_id;
    const instituicao   = req.body.instituicao;
    const boleto        = req.body.boleto;
    // const valor_total   = req.body.valor_total; //valor tem que ser com ponto ex: 100.50
    const parcelas      = req.body.parcelas;
    const vencido       = req.body.vencido;
    const curso         = req.body.curso;
    const created_at    = new Date();

    const { query } = await db.query(
        "INSERT INTO pgprv_proposta(pro_id, usr_id, instituicao, boleto, "+
        "parcelas, vencido, curso, created_at) "+
        "VALUES ($1, $2, $3, $4, $5, $6, $7, $8)",
        [pro_id, usr_id, instituicao, boleto, parcelas, vencido, curso, created_at], (err, result) => {
            if (err) {
                res.status(500).send(err);
                return console.error('Error executing query', err.stack);
            }
        })

    res.status(200).send("Proposta registrada com sucesso. Id = "+pro_id);
})

//altera proposta do boleto a ser parcelado
router.post('/update', async (req, res) => {
    const pro_id        = req.body.pro_id;
    const usr_id        = req.body.usr_id;
    const instituicao   = req.body.instituicao;
    const boleto        = req.body.boleto;
    // const valor_total   = req.body.valor_total; //valor tem que ser com ponto ex: 100.50
    const parcelas      = req.body.parcelas;
    const vencido       = req.body.vencido;
    const curso         = req.body.curso;
    const comprovante   = req.body.comprovante;
    const updated_at    = new Date();

    const { query } = await db.query(
        "UPDATE pgprv_proposta SET instituicao = $1, boleto = $2, "+
        "parcelas = $3, vencido = $4, curso = $5, comprovante = $6, updated_at = $7"+
        "WHERE pro_id = $8 OR usr_id = $9",
        [instituicao, boleto, parcelas, vencido, curso, comprovante, updated_at, pro_id, usr_id], (err, result) => {
            if (err) {
                res.status(500).send(err);
                return console.error('Error executing query', err.stack);
            }
        })

    res.status(200).send("Proposta alterada com sucesso.");
})